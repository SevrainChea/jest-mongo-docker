import Hero from './hero.model';

const createHero = async (heroProperties) => {
  try {
    return Hero.create(heroProperties);
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export { createHero };
