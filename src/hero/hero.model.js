import mongoose from 'mongoose';

const heroSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  height: {
    type: Number,
    required: true,
    min: [130, 'Must be at least 130cm, got {VALUE}'],
    max: [240, 'Can not exceed 240cm, got {VALUE}'],
  },
  weight: {
    type: Number,
    required: true,
    min: [25, 'Must be at least 25kg, got {VALUE}'],
    max: [140, 'Can not exceed 140kg, got {VALUE}'],
  },
  heroClass: {
    type: String,
    required: true,
    enum: ['ARCHER', 'WARRIOR', 'MAGE', 'CLOWN'],
  },
});

heroSchema.index({ name: 1, heroClass: 1 }, { unique: true });

const Hero = mongoose.model('Hero', heroSchema);

export default Hero;
