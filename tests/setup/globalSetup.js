import { exec } from 'child_process';
import mongoose from 'mongoose';

const environmentKeysToDelete = [];

module.exports = async () => {
  // Make sure to erase sensible environment variables
  environmentKeysToDelete.forEach((key) => {
    delete process.env[key];
  });

  const isInCI = process.env.CI;

  if (!isInCI) {
    exec('docker rm -f mongo-test');
    exec(
      'docker run -d \
      --name mongo-test \
      -p 27017:27017 \
      -e MONGO_INITDB_ROOT_USERNAME=testuser \
      -e MONGO_INITDB_ROOT_PASSWORD=testpassword \
      -e MONGO_INITDB_DATABASE=testdb \
      mongo'
    );
  }

  const mongooseOpts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  };
  const host = isInCI ? process.env.MONGO_TEST_HOST : 'localhost';
  const uri = `mongodb://testuser:testpassword@${host}:27017/testdb?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`;

  let retry = 0;
  let connected = false;
  const connection = mongoose.createConnection(uri, mongooseOpts);

  connection.on('connected', () => {
    console.log('Successfully connected to MongoDB');
    connected = true;
  });

  while (!connected && retry < 30) {
    console.log('Connecting to MongoDB...');
    await new Promise((r) => setTimeout(r, 1000));
    retry++;
  }

  if (!connected) {
    throw new Error('Could not connect to mongo for integration tests');
  }

  connection.close();
};
