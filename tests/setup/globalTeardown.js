import { exec } from 'child_process';

module.exports = async () => {
  if (!process.env.CI) exec('docker rm -f mongo-test');
};
