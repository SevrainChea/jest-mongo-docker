import { dbConnect, dbDisconnect, dbClear } from '../utils';
import { createHero } from '../../src/hero/hero.service';

beforeAll(async () => await dbConnect());
afterEach(async () => await dbClear());
afterAll(async () => await dbDisconnect());

describe('Test HeroService createHero', () => {
  it('should create a new Hero', async () => {
    const heroResult = await createHero({
      name: 'Thor',
      height: 185,
      weight: 90,
      heroClass: 'WARRIOR',
    });

    expect(heroResult._id).toBeDefined();
    expect(heroResult.name).toBe('Thor');
    expect(heroResult.height).toBe(185);
    expect(heroResult.weight).toBe(90);
    expect(heroResult.heroClass).toBe('WARRIOR');
  });

  it('should fail because of invalid heroClass', async () => {
    await expect(
      createHero({
        name: 'Thor',
        height: 185,
        weight: 90,
        heroClass: 'INVALID',
      })
    ).rejects.toThrow(
      'Hero validation failed: heroClass: `INVALID` is not a valid enum value for path `heroClass`.'
    );
  });
});
