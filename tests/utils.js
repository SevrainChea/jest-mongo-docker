import mongoose from 'mongoose';

const dbConnect = async () => {
  const host = process.env.CI ? process.env.MONGO_TEST_HOST : 'localhost';
  const uri = `mongodb://testuser:testpassword@${host}:27017/testdb?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false`;

  const mongooseOpts = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoCreate: true,
  };
  mongoose.connect(uri, mongooseOpts);
};

const dbDisconnect = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
};

const dbClear = async () => {
  const collections = Object.values(mongoose.connection.collections);
  await Promise.all(collections.map((collection) => collection.deleteMany()));
};

export { dbConnect, dbDisconnect, dbClear };
