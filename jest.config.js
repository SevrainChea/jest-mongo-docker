const config = {
  globalSetup: './setup/globalSetup.js',
  globalTeardown: './setup/globalTeardown.js',
  rootDir: './tests',
  testEnvironment: 'node',
};

module.exports = config;
